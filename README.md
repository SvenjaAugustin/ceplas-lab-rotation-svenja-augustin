Author: Svenja Augustin

Date: 17th March - 27th April 2021

CEPLAS Grad School laboratory rotation

Supervisor: Dr Anna Matuszyńska

Organization: Institute for Quantitative and Theoretical Biology at the Heinrich-Heine University, Düsseldorf, Germany

Contact: please direct any questions regarding the herein published code at Svenja.Augustin@hhu.de 

Description: Describes gene regulatory networks within the shoot apical meristem of Arabidopsis thaliana on a single cell level. Partial reproduction and extension of the published mathematical model by Liu et al. (2020, https://doi.org/10.1016/j.csbj.2020.11.017) implementing six ordinary differential equation within the Python-based modelbase software by van Aalst et al. (2021, https://doi.org/10.1186/s12859-021-04122-7). 
